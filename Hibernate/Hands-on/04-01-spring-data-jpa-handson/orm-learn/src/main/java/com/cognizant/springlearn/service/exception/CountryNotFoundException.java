package com.cognizant.springlearn.service.exception;

/**
 * @author AVINASH
 *
 */

public class CountryNotFoundException extends Exception {

	public CountryNotFoundException(String str) {
		super(str);
	}
}
