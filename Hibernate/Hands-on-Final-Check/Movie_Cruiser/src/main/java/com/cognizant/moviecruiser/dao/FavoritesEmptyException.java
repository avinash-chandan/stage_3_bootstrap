package com.cognizant.moviecruiser.dao;

/**
 * @author AVINASH
 *
 */

public class FavoritesEmptyException extends Exception {

	private static final long serialVersionUID = 1L;

	public FavoritesEmptyException() {
		super();
	}

}
