package com.cognizant.truyum.dao;

/**
 * @author AVINASH
 *
 */

public class CartEmptyException extends Exception {

	private static final long serialVersionUID = 1L;

	public CartEmptyException() {
		super();
	}

}
