
/**
 * @author AVINASH
 *
 */

public interface MovableAdapter {
    // returns speed in KM/H
    double getSpeed();
}
