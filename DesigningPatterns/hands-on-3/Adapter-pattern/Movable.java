
/**
 * @author AVINASH
 *
 */

public interface Movable {
    // returns speed in MPH
    double getSpeed();
}
