package com.cognizant.designPatterns;

/**
 * @author AVINASH
 *
 */

public class PhoneOrder implements IOrder{

	@Override
	public void ProcessOrder(String modelName) {
		System.out.println(modelName + " order accepted!");
	}

}
