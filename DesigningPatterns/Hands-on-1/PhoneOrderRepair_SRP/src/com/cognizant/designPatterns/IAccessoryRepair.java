package com.cognizant.designPatterns;

/**
 * @author AVINASH
 *
 */


public interface IAccessoryRepair {
	void ProcessAccessoryRepair(String accessoryType);
}
