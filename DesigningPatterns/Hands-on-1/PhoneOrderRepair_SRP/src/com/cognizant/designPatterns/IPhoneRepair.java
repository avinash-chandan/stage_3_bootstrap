package com.cognizant.designPatterns;

/**
 * @author AVINASH
 *
 */

public interface IPhoneRepair {
	void ProcessPhoneRepair(String modelName);

}
