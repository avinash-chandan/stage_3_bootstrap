package com.cognizant.designPatterns;

/**
 * @author AVINASH
 *
 */

public interface IOrder {
	void ProcessOrder(String modelName);
}
