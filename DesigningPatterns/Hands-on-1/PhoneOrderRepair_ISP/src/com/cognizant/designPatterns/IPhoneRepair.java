package com.cognizant.designPatterns;

/**
 * @author AVINASH
 *
 */

public interface IPhoneRepair extends IRepair{
	void ProcessPhoneRepair(String modelName);
}
