package com.cognizant.designPatterns;

/**
 * @author AVINASH
 *
 */

public interface IAccessoryRepair extends IRepair{
	void ProcessAccessoryRepair(String accessoryName);
}
