import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {


    static int[] rotateArray(int[] arr, int k){
        int len = arr.length;
        k = k % len;
        
        if(k == 0){
            return arr;
        } else {
            int temp[] = new int[k];
            for(int i=0;i<k;i++){
                temp[i] = arr[i];
            }

            for(int i=k;i<len;i++){
                arr[i-k] = arr[i];
            }

            for(int i=len-k;i<len;i++){
                arr[i] = temp[i-len+k];
            }
            return arr;
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] nd = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nd[0]);

        int d = Integer.parseInt(nd[1]);

        int[] a = new int[n];

        String[] aItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int aItem = Integer.parseInt(aItems[i]);
            a[i] = aItem;
        }

        scanner.close();

        int ans[] = rotateArray(a,d);
        for(int i=0;i<n;i++){
            System.out.print(ans[i]+" ");
        }
    }
}

